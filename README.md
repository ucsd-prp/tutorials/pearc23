# Scientific Computing with Kubernetes

An upcoming tutorial at PEARC23.
To be presented by Mahidhar Tatineni, Igor Sfiligoi and Dima Mishin.

## Overview

Kubernetes has become the leading container orchestration solution over the past few years. It can work anywhere from on-prem clusters to commercial clouds, abstracting the computational resources and workloads to run on those.

The main compute paradigm for large-scale distributed computing has long been the batch system. Kubernetes doesn't directly present a traditional batch interface, but the concepts are similar enough to allow for easy porting of existing batch-focused workloads to it. Kubernetes additionally provides a significantly richer semantics, including explicit storage and network provisioning, that allows for compute workloads previously not feasible on traditional batch system.

The main aim of this tutorial is to show how one can transition batch-oriented scientific workflows into Kubernetes.

## Agenda

```
Introduction and Welcome
An overview of the Kubernetes architecture (Lecture)
Basic Kubernetes Hands On
User applications with Kubernetes (Lecture)
Dealing with data Hands On - Part 1
Break
Dealing with data Hands On - Part 2
Kubernetes resource scheduling (Lecture)
Scheduling Hands On
Monitoring your compute (Lecture)
Additional tools in Kubernetes (Lecture)
Closeout
```

